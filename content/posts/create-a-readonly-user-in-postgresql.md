+++
title = "Create a Readonly User in Postgresql"
date = "2021-12-10T15:23:40+01:00"
author = ""
authorTwitter = "" #do not include @
cover = ""
tags = ["postgresql", "roles", "readonly"]
keywords = ["postgres", "postgresql", "readonly"]
description = ""
showFullContent = false
readingTime = false
+++

## Create the Read-Only user:

[Link to](http://www.dbrnd.com/2015/10/postgresql-script-to-create-a-read-only-database-user/) source.
[Stackoverflow](https://stackoverflow.com/questions/760210/how-do-you-create-a-read-only-user-in-postgresql)

When we're done the user will __only__ be able to `SELECT` table data, and at the same time be restricted by schema access.
Make sure to change `schema` according to your needs.

```sql
CREATE ROLE Read_Only_User WITH LOGIN PASSWORD 'Test1234'
NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';
```

---

## Assign permissions to the read only user:

```sql
GRANT CONNECT ON DATABASE YourDatabaseName TO Read_Only_User;
GRANT USAGE ON SCHEMA public TO Read_Only_User;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO Read_Only_User;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO Read_Only_User;
```

Restriction for user by database schema, and only `SELECT` permission for tables.

---

Would you need permission for functions:

```sql
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO Read_Only_User;
```

---

## List Tables and Roles with Access

```sql
select grantee, table_catalog, table_schema, table_name, string_agg(privilege_type, ', ' order by privilege_type) as privileges from information_schema.role_table_grants where grantee != 'postgres' group by grantee, table_catalog, table_schema, table_name;
```

![alt text](https://i.postimg.cc/4ycxQ1rG/image.png "list tables and roles with access")
